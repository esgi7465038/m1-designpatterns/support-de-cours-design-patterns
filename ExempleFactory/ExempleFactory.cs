﻿using Factory;
using Menu;

Console.WriteLine("*************************");
Console.WriteLine("* Menu avec PlatFactory *");
Console.WriteLine("*************************");

//  Création de la fabrique
PlatFactory factory = new PlatFactory();

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entree = factory.CreateEntree("Salade verte");
PlatPrincipal platPrincipal = (PlatPrincipal)factory.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(factory.CreateDessert("Tiramizu"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { factory.CreateEntree("Nems"),
    factory.CreateEntree("Giozza"),
    factory.CreatePlatPrincipal("Bobun"),
    factory.CreateDessert("Moshi"),
    factory.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());

