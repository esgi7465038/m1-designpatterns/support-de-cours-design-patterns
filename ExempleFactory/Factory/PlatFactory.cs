﻿using Menu;

namespace Factory
{
    internal class PlatFactory
    {
        public IPlat CreateEntree(string nom)
        {
            return new Entree(nom);
        }

        public IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipal(nom);
        }

        public IPlat CreateDessert(string nom)
        {
            return new Dessert(nom);
        }
    }
}
