﻿namespace Observateur
{
    public interface ISujet
    {
        /// <summary>
        /// Ajoute un observateur à la liste.
        /// </summary>
        /// <param name="observateur">L'observateur à ajouter</param>
        public void AjouteObservateur(IObservateur observateur);

        /// <summary>
        /// Enlève un observateur de la liste.
        /// </summary>
        /// <param name="observateur">>L'observateur à retirer</param>
        public void RetireObservateur(IObservateur observateur);

        /// <summary>
        /// Notifie le changement à tous les observateurs de la liste
        /// </summary>
        protected void NotifierObservateurs();
    }
}
