﻿namespace Etats
{
    class TransitionImpossibleException : Exception
    {
        public TransitionImpossibleException()
            : base("Impossible")
        {
        }

        public TransitionImpossibleException(String msg)
            : base(msg)
        {
        }
    }
}
