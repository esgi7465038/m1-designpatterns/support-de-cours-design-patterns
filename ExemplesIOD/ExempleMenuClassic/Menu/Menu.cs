﻿namespace Menu
{
    internal class Menu
    {
        private readonly int nTable;
        protected Entree entree;
        protected PlatPrincipal platPrincipal;
        protected Dessert dessert;

        public Menu(int nTable, string nomEntree, string nomPlatPrincipal, string nomDessert) 
        { 
            this.nTable = nTable;
            entree = new Entree(nomEntree);
            platPrincipal = new PlatPrincipal(nomPlatPrincipal);
            dessert = new Dessert(nomDessert);
        }

        public string Consommer()
        {
            string conso = "La table " + nTable + " est servie.\n";
            conso += entree.Manger();
            conso += platPrincipal.Manger();
            conso += dessert.Manger();

            return conso;
        }
    }
}
