﻿namespace PersonneMarcherCourrir
{
    internal class Personne
    {
        public string Nom { get; }
        public int Age { get; }

        public Personne(string nom, int age)
        {
            Nom = nom;
            Age = age;
        }

        public string Marcher()
        {
            return Nom + " se déplace en marchant.";
        }

        public string Courrir() => Nom + " se déplace en courant.";
    }
}
