﻿namespace PersonneStrategyDeplacement
{
    internal class Courrir: IStrategyDeplacement
    {
        public string DoDeplacement()
        {
            return " se déplace en courrant.";
        }
    }
}
