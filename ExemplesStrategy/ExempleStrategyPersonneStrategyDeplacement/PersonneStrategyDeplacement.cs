﻿using PersonneStrategyDeplacement;

Personne moi = new Personne("Phil", 45);

Marcher deplacementEnMarchant = new();
Courrir deplacementEnCourrant = new();

moi.Deplacement = deplacementEnMarchant;
Console.WriteLine(moi.SeDeplacer());

moi.Deplacement = deplacementEnCourrant;
Console.WriteLine(moi.SeDeplacer());

moi.Deplacement = deplacementEnMarchant;
Console.WriteLine(moi.SeDeplacer());
