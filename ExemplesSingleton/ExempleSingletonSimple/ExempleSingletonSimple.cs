﻿using ExempleSingletonSimple;

Console.WriteLine("----> Une classe 'normale'");
//  CPasSingleton n'implémente pas le DP Singleton...
CPasSingleton Obj_1 = new CPasSingleton();
CPasSingleton Obj_2 = new();
CPasSingleton Obj_3 = Obj_1;

//  On voit que les identifiants des objets sont différents.
Console.WriteLine("Obj_1: " + Obj_1.UnMessage);
Console.WriteLine("Obj_2: " + Obj_2.UnMessage);
Console.WriteLine("Obj_3: " + Obj_3.UnMessage);

Console.WriteLine("\n\n----> Une classe implémentant le DP 'Singleton'");
//  On ne peut pas instancier directement d'objet de classe CSingleton
//  CSingleton Single_0 = new CSingleton();

//  Il faut passer par la méthode (ici une property...) ObjetUnique
CSingleton Single_1 = CSingleton.ObjetUnique;
CSingleton Single_2 = CSingleton.ObjetUnique;
CSingleton Single_3 = Single_1;

//  Les identifiants des 3 références sont identiques => c'est le même objet
Console.WriteLine("Single_1: " + Single_1.UnMessage);
Console.WriteLine("Single_2: " + Single_2.UnMessage);
Console.WriteLine("Single_3: " + Single_3.UnMessage);
