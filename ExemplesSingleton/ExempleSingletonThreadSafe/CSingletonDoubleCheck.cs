﻿namespace ExempleSingletonThreadSafe
{
    internal class CSingletonDoubleCheck
    {
        //  La référence statique de la seule instance
        private static volatile CSingletonDoubleCheck? objetUnique = null;

        //  Une propriété.
        public string UnMessage => "JE SUIS UNIQUE! La preuve avec mon HashCode: " + this.GetHashCode().ToString();

        //  Le constructeur privé
        private CSingletonDoubleCheck()
        {
            Console.WriteLine("Constructeur " + UnMessage);
            Thread.Sleep(313);
        }

        //  La méthode d'accès à l'instance objetUnique
        public static CSingletonDoubleCheck ObjetUnique
        {
            get
            {
                //  Le premier "check" qui permet de ne pas faire systématiquement le lock.
                if (objetUnique == null)
                {
                    lock (typeof(CSingletonDoubleCheck))
                    {
                        //  Le second test, en section critique
                        //  Si objetUnique n'existe pas, on le crée
                        objetUnique ??= new CSingletonDoubleCheck();
                    }
                }
                //  On retourne l'instance existante, éventuellement fraichement crée.
                return objetUnique;
            }
        }
    }
}
