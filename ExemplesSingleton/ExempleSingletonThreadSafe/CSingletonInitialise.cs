﻿namespace ExempleSingletonThreadSafe
{
    internal class CSingletonInitialise
    {
        //  La référence statique de la seule instance et sa property
        private static readonly CSingletonInitialise objetUnique = new();
        public static CSingletonInitialise ObjetUnique => objetUnique;

        //  Une propriété.
        public string UnMessage => "JE SUIS UNIQUE! La preuve avec mon HashCode: " + this.GetHashCode().ToString();

        //  Le constructeur privé
        private CSingletonInitialise()
        {
            Console.WriteLine("Constructeur " + UnMessage);
            Thread.Sleep(313);
        }
    }
}
