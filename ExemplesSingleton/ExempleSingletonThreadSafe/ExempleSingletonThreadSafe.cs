﻿namespace ExempleSingletonThreadSafe
{
    internal class ExempleSingletonThreadSafe
    {
        /// <summary>
        /// Pour arréter les threads...
        /// </summary>
        private bool stopThread = false;

        static void Main(string[] args)
        {

            ExempleSingletonThreadSafe prog = new();

            //  Création de 3 threads.
            Thread thread_0 = new(prog.Run_0);
            Thread thread_1 = new(prog.Run_1);
            Thread thread_2 = new(prog.Run_2);

            Console.WriteLine("---->  Lancement des threads");
            thread_0.Start();
            thread_1.Start();
            thread_2.Start();

            Console.WriteLine("---->  Threads en cours");
            Thread.Sleep(2000);

            Console.WriteLine("---->  Arrêts des threads");
            prog.stopThread = true;

            thread_0.Join();
            thread_1.Join();
            thread_2.Join();
            Console.WriteLine("---->  Fin des threads");
        }

        //  Thread 0
        public void Run_0()
        {
            //CSingleton Single = CSingleton.ObjetUnique;
            CSingletonInitialise Single = CSingletonInitialise.ObjetUnique;
            //CSingletonDoubleCheck Single = CSingletonDoubleCheck.ObjetUnique;
            Thread.Sleep(337);
            Console.WriteLine("Thread 0, Single: " + Single.UnMessage);
            while (!stopThread) ;
            Console.WriteLine("Thread 0 terminée");
        }

        //  Thread 1
        public void Run_1()
        {
            //CSingleton Single = CSingleton.ObjetUnique;
            CSingletonInitialise Single = CSingletonInitialise.ObjetUnique;
            //CSingletonDoubleCheck Single = CSingletonDoubleCheck.ObjetUnique;
            Thread.Sleep(251);
            Console.WriteLine("Thread 1, Single: " + Single.UnMessage);
            while (!stopThread) ;
            Console.WriteLine("Thread 1 terminée");
        }

        //  Thread 2
        public void Run_2()
        {
            //CSingleton Single = CSingleton.ObjetUnique;
            CSingletonInitialise Single = CSingletonInitialise.ObjetUnique;
            //CSingletonDoubleCheck Single = CSingletonDoubleCheck.ObjetUnique;
            Thread.Sleep(223);
            Console.WriteLine("Thread 2, Single: " + Single.UnMessage);
            while (!stopThread) ;
            Console.WriteLine("Thread 2 terminée");
        }
    }
}