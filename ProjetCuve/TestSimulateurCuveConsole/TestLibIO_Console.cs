﻿using LibIO;
using System;
using System.Threading;


namespace TestLibIO_Console
{
    class TestLibIO_Console
    {
        static void Main(string[] args)
        {
            //  L'objet pour communiquer avec le serveur distant d'acquisition
            CLibIO io;

            //  Les information sur le serveur distant d'acquisition
            string nomServeur;
            int portServeur;
            int portClient;

            //  Le booléen pour les sorties ToR
            bool sTor = false;

            uint compteur = 0;

            //  Soit on récupère les 3 arguments sur la ligne de commande,
            //  soit on utilise les paramètres par défaut.
            if (args.Length == 3)
            {
                nomServeur = args[0];
                portServeur = int.Parse(args[1]);
                portClient = int.Parse(args[2]);
            }
            else
            {
                nomServeur = "127.0.0.1";
                portServeur = 1069;
                portClient = 0;
            }

            try
            {
                //  instanciation des IO
                Console.Write("Instanciation LibIO({0},{1},{2})", nomServeur, portServeur, portClient);
                io = new CLibIO(nomServeur, portServeur, portClient);

                // Il faut laisser un peu de temps à la communication pour s'établir...
                Thread.Sleep(1000);
                Console.WriteLine("====> START");

                while (true)
                {
                    Console.WriteLine("===> Boucle {0}", ++compteur);

                    //  Acquisition des capteurs ToR
                    Console.Write("Capteurs Tor:");
                    for (uint i = 0; i < MapIO.NB_CPT_TOR; i++)
                    {
                        Console.Write(" N° " + i + ": " +io.get_entreeToR_Nb(i) + ",");
                    }
                    Console.WriteLine();

                    //  Acquisition des capteurs AN
                    Console.Write("Capteurs AN:");
                    for (uint i = 0; i < MapIO.NB_CPT_An; i++)
                    {
                        Console.Write(" N° " + i + ": " + io.get_entreeAn_Nb(i) + ",");
                    }
                    Console.WriteLine();

                    //  Affectation des sorties ToR
                    Console.WriteLine("Sorties ToR: 0={0}\t1={1}", sTor, !sTor);
                    io.set_sortieToR_Nb(0, sTor);
                    io.set_sortieToR_Nb(1, !sTor);
                    sTor = !sTor;

                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur... Pas de serveur ?");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Syntaxe: ConsoleTestSim IPServeur(def: 127.0.0.1) PortServeur(def: 1069) POrtClient(def: 0)");
            }
        }
    }
}
