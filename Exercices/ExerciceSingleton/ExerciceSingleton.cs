﻿using ExerciceSingleton;

Console.WriteLine("Instanciation du sujet concret");
SujetConcret sujet = SujetConcret.ObjetUnique;
SujetConcret sujet2 = SujetConcret.ObjetUnique;

if(ReferenceEquals(sujet, sujet2))
{
    Console.WriteLine("--> sujet et sujet2 ont la même référence: " + sujet.GetHashCode() + " " + sujet2.GetHashCode());
}
else
{
    Console.WriteLine("--> Oups... sujet et sujet2 n'ont pas la même référence: " + sujet.GetHashCode() + " " + sujet2.GetHashCode());
}

Console.WriteLine("Modification consigne du sujet");
sujet.Consigne = 80;
Thread.Sleep(3000);

//  Instanciation des observateurs
ObservateurConcret obs1 = new(ConsoleColor.Green);
ObservateurConcret obs2 = new(ConsoleColor.Red);
ObservateurAlerte obsAlerte = new();

Console.WriteLine("Inscription obs1: ");
sujet.AjouteObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(7000);

Console.WriteLine("Inscription obs2 et alerte: ");
sujet2.AjouteObservateur(obs2);
sujet2.AjouteObservateur(obsAlerte);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(7000);

Console.WriteLine("Modification consigne du sujet");
sujet2.Consigne = 95;
Thread.Sleep(12000);

Console.WriteLine("Retrait  obs1: ");
sujet.RetireObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
sujet.Consigne = 50;
Thread.Sleep(15000);

sujet2.Stop = true;
sujet2.Join();

Console.WriteLine("Sujet arrété");
sujet.RetireObservateur(obs2);
sujet.RetireObservateur(obsAlerte);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
