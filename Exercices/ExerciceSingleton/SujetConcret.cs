﻿using Observateur;

namespace ExerciceSingleton
{
    internal class SujetConcret : SujetAbstrait
    {
        private static readonly SujetConcret objetUnique = new();
        public static SujetConcret ObjetUnique => objetUnique;

        /// <summary>
        /// La valeur qui sera observée par les observateurs
        /// </summary>
        public int ValObservee { get; private set; } = 0;


        /// <summary>
        /// Une consigne donnée que la valeur observée cherchera à rejoindre.
        /// </summary>
        private int consigne = 0;

        public int Consigne
        {
            get => consigne;
            set
            {
                if (value < 0)
                    consigne = 0;
                else if (value > 100)
                    consigne = 100;
                else
                    consigne = value;
            }
        }

        private readonly Thread LaTache;
        public bool Stop { get; set; }

        private SujetConcret(int startVal = 50)
        {
            ValObservee = startVal;
            LaTache = new(Simulation);
            LaTache.Start();
        }

        private void Simulation()
        {
            while (!Stop)
            {
                if (ValObservee != consigne)
                {
                    //  Mise à jour de la valeur observée
                    if (ValObservee < consigne)
                        ValObservee++;
                    else
                        ValObservee--;

                    //  On prévient tous les observateurs
                    this.NotifierObservateurs();
                }
                Thread.Sleep(500);
            }
        }

        public void Join()
        {
            LaTache.Join(); 
        }
    }
}
