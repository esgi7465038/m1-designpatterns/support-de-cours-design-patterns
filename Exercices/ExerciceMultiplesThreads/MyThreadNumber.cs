﻿using System.Diagnostics;

namespace SimpleThreadingExample
{
    /// <summary>
    /// Classe encapsulant un thread paramétré affichant en boucle des nombres entiers.
    /// </summary>
    class MyThreadNumber
    {
        #region attributs & propriétés
        /// <summary>
        /// Un random pour le sleep...
        /// </summary>
        private readonly Random rnd = new();

        /// <summary>
        /// Un booléen pour arréter le thread.
        /// </summary>
        public bool Stop { set { stop = value; } }
        private volatile bool stop = false;

        /// <summary>
        /// Le nombre à afficher
        /// </summary>
        public int Number { get; private set; } = 0;

        /// <summary>
        /// Le thread lui même
        /// </summary>
        public Thread LaTache { get; }
        #endregion

        #region méthodes
        /// <summary>
        /// Le constructeur.
        /// Il instancie le thread.
        /// </summary>
        public MyThreadNumber()
        {
            //  Création d'un thread paramétré, sans le démmarer
            LaTache = new Thread(new ParameterizedThreadStart(RunNumber));
        }

        /// <summary>
        /// Pour démarrer le thread
        /// </summary>
        /// <param name="max">la limite max du comptage dans le thread</param>
        public void Start(int max = 999)
        {
            try
            {
                LaTache.Start(max);
                Console.WriteLine("Number démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number a déjà été demarré...");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer par manque de mémoire...");
                throw ex;
            }
        }

        #endregion

        #region thread
        /// <summary>
        /// La méthode s'exécutant en tant que Thread.
        /// </summary>
        /// <param name="max">la limite max du comptage</param>
        private void RunNumber(object? max)
        {
            //  Pour mesurer le temps
            var sw = Stopwatch.StartNew();

            Console.WriteLine("\t=======> Le RunNumber commence son travail, id {0}, état {1}, priorité {2}.",
                        Environment.CurrentManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            while (!stop)
            {
                if (max !=null && Number < (int)max)
                    Number++;
                else
                    Number = 0;

                Console.WriteLine("\t=======> Number: " + Number.ToString() + " à " + sw.ElapsedMilliseconds / 1000.0 + " secondes.");

                //  Dodo pour un temps aléatoire 20 < sleep <= 323ms
                Thread.Sleep(rnd.Next(20, 323));
            }
            Console.WriteLine("\t=======> Le threadNumber a fini son travail.");
        }
        #endregion
    }
}
