﻿using System.Diagnostics;

namespace SimpleThreadingExample
{
    /// <summary>
    /// Classe encapsulant un thread affichant en boucle les lettres de l'alphabet.
    /// </summary>
    class MyThreadLetter
    {
        #region attributs & propriétés
        /// <summary>
        /// Un random pour le sleep...
        /// </summary>
        private readonly Random rnd = new();

        /// <summary>
        /// Un booléen pour arréter le thread.
        /// volatile car il peut être accédé depuis un autre thread...
        /// </summary>
        public bool Stop { set { stop = value; } }
        private volatile bool stop = false;

        /// <summary>
        /// La lettre à afficher
        /// </summary>
        public char Letter { get; private set; } = 'a';

        /// <summary>
        /// Le thread lui même
        /// </summary>
        public Thread LaTache { get; }
        #endregion

        #region méthodes
        /// <summary>
        /// Le constructeur.
        /// Il instancie le thread.
        /// </summary>
        public MyThreadLetter()
        {
            //  Création d'un thread, sans le démarrer
            LaTache = new Thread(new ThreadStart(RunLetter));
        }

        /// <summary>
        /// Pour démarrer le thread.
        /// </summary>
        public void Start()
        {
            try
            {
                LaTache.Start();
                Console.WriteLine("Letter démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Letter a déjà été demarré...");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Letter ne peut pas démarrer par manque de mémoire...");
                throw ex;
            }
        }
        #endregion

        #region thread
        /// <summary>
        /// La méthode s'exécutant en tant que Thread.
        /// </summary>
        private void RunLetter()
        {
            //  Pour mesurer le temps
            var sw = Stopwatch.StartNew();

            Console.WriteLine("\t\t---> Le RunLetter commence son travail, id {0}, état {1}, priorité {2}.",
                        Environment.CurrentManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            while (!stop)
            {
                if (Letter < 'z')
                    Letter++;
                else
                    Letter = 'a';

                Console.WriteLine("\t\t---> Letter: " + Letter.ToString() + " à " + sw.ElapsedMilliseconds / 1000.0 + " secondes.");

                //  Dodo pour un temps aléatoire 50 < sleep <= 1267ms
                Thread.Sleep(rnd.Next(50, 1267));
            }
            Console.WriteLine("\t\t---> Le threadLetter a fini son travail.");
        }
        #endregion
    }
}
