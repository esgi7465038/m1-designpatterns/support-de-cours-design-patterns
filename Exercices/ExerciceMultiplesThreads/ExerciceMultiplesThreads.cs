﻿using SimpleThreadingExample;

namespace ExempleMultiplesThreads
{
    internal class ExempleMultiplesThreads
    {
        private int nbIteration = 15;
        private int compteurIteration = 0;

        static void Main(string[] args)
        {
            ExempleMultiplesThreads prog = new();
            Console.WriteLine("Au revoir!!");
        }

        public ExempleMultiplesThreads()
        {
            //  Création des objets encapsulant les threads.
            MyThreadLetter ThLetter = new();
            MyThreadNumber ThCptr = new();
            Console.WriteLine("> Les thread sont créées.\n> Démarrage...");

            //  Un Event pour se synchroniser avec le callback du Timer 
            AutoResetEvent compteurIterationEvent = new(false);

            Console.WriteLine("> {0:hh:mm:ss.ff} Creation du timer.", DateTime.Now);
            //  Le Timer qui invoque PrincipalWork chaque seconde, après un delais de 2sec
            Timer principalWorkTimer = new Timer(callback: PrincipalWork, 
                                                 state:compteurIterationEvent, 
                                                 dueTime:2000, 
                                                 period:1000);

            //  Démarrage des threads
            ThLetter.Start();
            //  RunNumber() de MyThreadNumber reçoit un paramètre... 
            ThCptr.Start(19);

            // Quand compteurIterationEvent est signalé, change le rythme du timer à 0.5 sec.
            compteurIterationEvent.WaitOne();
            principalWorkTimer.Change(0, 500);
            Console.WriteLine("> Change la periode à 0.5 seconde.");

            // When compteurIterationEvent signals the second time, dispose of the timer.
            compteurIterationEvent.WaitOne();
            Console.WriteLine("> Destruction du timer.");
            principalWorkTimer.Dispose();

            //  Arrêt des threads
            Console.WriteLine("> Demmande d'arret des threads.");
            ThLetter.Stop = true;
            ThCptr.Stop = true;

            //  On attend que les threads en cours soient effectivement terminés avant de continuer.
            ThLetter.LaTache.Join();
            ThCptr.LaTache.Join();
            Console.WriteLine("> Les threads sont terminés.");

            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();
        }

        private void PrincipalWork(Object? stateInfo)
        {
            AutoResetEvent? autoEvent = stateInfo as AutoResetEvent;
            Console.WriteLine("> {0} PrincipalWork {1,2}.",
                DateTime.Now.ToString("hh:mm:ss.ff"),
                (++compteurIteration).ToString());

            if (compteurIteration == nbIteration)
            {
                //  Reset le compteur et lève le signal
                compteurIteration = 0;
                autoEvent.Set();
            }
        }
    }
}
