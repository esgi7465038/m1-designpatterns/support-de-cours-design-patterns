﻿using Etats;


//  Création des états concrets possibles
IEtatEtudiant[] LesEtats;
LesEtats = new IEtatEtudiant[3];
LesEtats[Etudiant.etatHeureux] = new EtatHeureux();
LesEtats[Etudiant.etatContrarie] = new EtatContrarie();
LesEtats[Etudiant.etatDepite] = new EtatDepite();

Etudiant UnEtudiant = new Etudiant("Pal", LesEtats);

//  Etat Initial -> Heureux
Console.WriteLine("1)" + UnEtudiant.DireBonjour());

UnEtudiant.Travailler(true);
//  Pas de transition, reste Heureux
Console.WriteLine("2)" + UnEtudiant.DireBonjour());

UnEtudiant.PartirEnVacance();
//  Pas de transition, reste Heureux
Console.WriteLine("3)" + UnEtudiant.DireBonjour());

UnEtudiant.RentrerDeVacance();
//  --> Contrarié
Console.WriteLine("4)" + UnEtudiant.DireBonjour());

UnEtudiant.Travailler(true);
//  --> Dépité
Console.WriteLine("5)" + UnEtudiant.DireBonjour());

try
{
    //  Lance une exception TransitionImpossibleException
    UnEtudiant.RentrerDeVacance();
    Console.WriteLine("6)" + UnEtudiant.DireBonjour());
}
catch (TransitionImpossibleException ex)
{
    Console.WriteLine("Exception Transition Impossible: " + ex.Message);
}
finally
{
    //  Toujours Dépité
    Console.WriteLine("7)" + UnEtudiant.DireBonjour());
}

UnEtudiant.Travailler(true);
//  Ne provoque pas de transition
Console.WriteLine("8)" + UnEtudiant.DireBonjour());

UnEtudiant.Travailler(false);
//  --> Contrarié
Console.WriteLine("9)" + UnEtudiant.DireBonjour());

UnEtudiant.Travailler(false);
//  Ne provoque pas de transition
Console.WriteLine("10)" + UnEtudiant.DireBonjour());

UnEtudiant.PartirEnVacance();
//  --> Heureux
Console.WriteLine("11)" + UnEtudiant.DireBonjour());

UnEtudiant.RentrerDeVacance();
UnEtudiant.Travailler(true);
//  --> Dépité
Console.WriteLine("12)" + UnEtudiant.DireBonjour());
UnEtudiant.PartirEnVacance();
//  --> Heureux
Console.WriteLine("13)" + UnEtudiant.DireBonjour());
