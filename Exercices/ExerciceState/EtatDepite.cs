﻿namespace Etats
{
    class EtatDepite : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "Pfff Ggrrrrrr !!!";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Rahh Lovely! J'étais Dépité, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            throw new TransitionImpossibleException("Pff, J'suis déjà Dépité...");
        }

        public bool Travailler(Etudiant contexte, bool aFaire)
        {
            if (!aFaire)
            {
                Console.WriteLine("J'étais Dépité, maintenant je ne suis que Contrarié...");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
            }
            return aFaire;
        }
    }
}
