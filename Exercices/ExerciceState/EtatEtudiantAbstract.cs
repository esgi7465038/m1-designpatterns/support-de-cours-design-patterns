﻿namespace Etats;

internal abstract class EtatEtudiantAbstract : IEtatEtudiant
{
    public string DireBonjour(Etudiant contexte)
    {
        throw new TransitionImpossibleException("Transition impossible.");
    }

    public void PartirEnVacance(Etudiant contexte)
    {
        throw new TransitionImpossibleException("Transition impossible.");
    }

    public void RentrerDeVacance(Etudiant contexte)
    {
        throw new TransitionImpossibleException("Transition impossible.");
    }

    public bool Travailler(Etudiant contexte, bool aFaire)
    {
        throw new TransitionImpossibleException("Transition impossible.");
    }
}
