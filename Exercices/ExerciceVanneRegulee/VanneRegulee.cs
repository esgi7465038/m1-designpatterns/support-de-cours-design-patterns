﻿namespace Vannes;

/// <summary>
/// La classe VanneRegulee hérite de Vanne et implémente l'interface IRegulable
/// </summary>
public class VanneRegulee : Vanne, IRegulable
{
    public bool IsReguleOn { get; set; }

    public void Regule(int consigneDebit)
    {
        Position = (consigneDebit <= debitMax) ? consigneDebit * 100 / debitMax : 100;
    }

    public VanneRegulee(bool isReguleOn = true)
    {
        IsReguleOn = isReguleOn;
    }
}
