﻿using Factory;
using Menu;

Console.WriteLine("*********************************");
Console.WriteLine("* Menu avec PlatFactoryAbstract *");
Console.WriteLine("*********************************");

//  Création de la fabrique pour plats adultes
PlatFactoryAbstract factory = new PlatAdulteFactory();

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
EntreeAdulte entree = (EntreeAdulte)factory.CreateEntree("Salade verte");
PlatPrincipalAbstract platPrincipal = factory.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(factory.CreateDessert("Tiramizu"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création de la fabrique pour plats enfant
factory = new PlatEnfantFactory();

//  Création d'un tableau de Iplat
IPlat[] plats = [ factory.CreateEntree("Oeuf mimosa"),
    factory.CreatePlatPrincipal("Nuggets de poulet"),
    factory.CreatePlatPrincipal("Frites"),
    factory.CreateDessert("Glace au chocolat")
];

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());

