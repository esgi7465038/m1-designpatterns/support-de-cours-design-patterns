﻿namespace Menu
{
    internal class DessertAdulte(string nom) : DessertAbstract(nom)
    {
        public override string Manger()
        {
            return "=> Adulte, je mange le dessert " + Nom + "\n";
        }
    }
}
