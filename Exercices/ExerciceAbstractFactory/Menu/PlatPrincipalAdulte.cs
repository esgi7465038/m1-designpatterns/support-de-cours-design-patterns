﻿namespace Menu
{
    internal class PlatPrincipalAdulte(string nom) : PlatPrincipalAbstract(nom)
    {
        public override string Manger()
        {
            return "=> Adulte, je mange le plat principal " + Nom + "\n";
        }
    }
}
