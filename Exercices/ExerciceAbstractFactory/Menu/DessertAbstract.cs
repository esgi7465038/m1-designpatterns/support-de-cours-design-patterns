﻿namespace Menu
{
    internal abstract class DessertAbstract(string nom) : IPlat
    {
        public readonly string Nom = nom;

        public abstract string Manger();
    }
}
