﻿namespace Menu
{
    internal abstract class EntreeAbstract(string nom) : IPlat
    {
        public readonly string Nom = nom;

        public abstract string Manger();
    }
}
