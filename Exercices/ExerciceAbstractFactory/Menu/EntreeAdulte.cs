﻿namespace Menu
{
    internal class EntreeAdulte : EntreeAbstract
    {
        public EntreeAdulte(string nom) : base(nom)
        { 
        }

        public override string Manger()
        {
            return "=> Adulte, je mange l'entrée " + Nom + "\n";
        }
    }
}
