﻿namespace Menu
{
    internal abstract class PlatPrincipalAbstract(string nom) : IPlat
    {
        public readonly string Nom = nom;

        public abstract string Manger();
    }
}
