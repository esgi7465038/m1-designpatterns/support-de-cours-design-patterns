﻿namespace Menu
{
    internal class EntreeEnfant : EntreeAbstract
    {
        public EntreeEnfant(string nom) : base(nom)
        { 
        }

        public override string Manger()
        {
            return "==> Enfant, je mange l'entrée " + Nom + "\n";
        }
    }
}
