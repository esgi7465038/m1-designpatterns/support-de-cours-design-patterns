﻿namespace Menu
{
    internal class PlatPrincipalEnfant(string nom) : PlatPrincipalAbstract(nom)
    {
        public override string Manger()
        {
            return "==> Enfant, je mange le plat principal " + Nom + "\n";
        }
    }
}
