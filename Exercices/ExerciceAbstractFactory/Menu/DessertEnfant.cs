﻿namespace Menu
{
    internal class DessertEnfant(string nom) : DessertAbstract(nom)
    {
        public override string Manger()
        {
            return "==> Enfant, je mange le dessert " + Nom + "\n";
        }
    }
}
