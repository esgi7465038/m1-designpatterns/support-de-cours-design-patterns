﻿using Menu;

namespace Factory
{
    internal abstract class PlatFactoryAbstract
    {
        public abstract EntreeAbstract CreateEntree(string nom);

        public abstract PlatPrincipalAbstract CreatePlatPrincipal(string nom);

        public abstract DessertAbstract CreateDessert(string nom);
    }
}
