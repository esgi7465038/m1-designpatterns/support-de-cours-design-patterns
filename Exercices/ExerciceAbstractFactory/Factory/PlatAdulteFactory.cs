﻿using Menu;

namespace Factory
{
    internal class PlatAdulteFactory : PlatFactoryAbstract
    {
        public override EntreeAbstract CreateEntree(string nom)
        {
            return new EntreeAdulte(nom);
        }

        public override PlatPrincipalAbstract CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalAdulte(nom);
        }

        public override DessertAbstract CreateDessert(string nom)
        {
            return new DessertAdulte(nom);
        }
    }
}
