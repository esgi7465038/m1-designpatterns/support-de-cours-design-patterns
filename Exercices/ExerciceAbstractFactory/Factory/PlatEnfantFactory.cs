﻿using Menu;

namespace Factory
{
    internal class PlatEnfantFactory : PlatFactoryAbstract
    {
        public override EntreeAbstract CreateEntree(string nom)
        {
            return new EntreeEnfant(nom);
        }

        public override PlatPrincipalAbstract CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalEnfant(nom);
        }

        public override DessertAbstract CreateDessert(string nom)
        {
            return new DessertEnfant(nom);
        }
    }
}
