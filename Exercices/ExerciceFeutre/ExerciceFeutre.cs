﻿using Feutres;

Feutres.Feutre unFeutreBleu;
Feutres.Feutre unAutreFeutreBleu = new("Plastique", ConsoleColor.Blue);
Feutres.Feutre unFeutreRouge;

Console.WriteLine("-----> 1");
unFeutreBleu = new Feutres.Feutre();
unFeutreBleu.Debouche();
unFeutreBleu.Ecrire("Hello les bleus");

Console.WriteLine("-----> 2");
unAutreFeutreBleu.Debouche();
unAutreFeutreBleu.Ecrire("Encore en bleu...");
unAutreFeutreBleu.Bouche();
if (!unAutreFeutreBleu.Ecrire("Tentative bouché..."))
{
    Console.WriteLine("-----> Un feutre ne peut pas écrire bouché...");
}

Console.WriteLine("-----> 3");
unFeutreRouge = new Feutres.Feutre("Métal", ConsoleColor.Red, false);
unFeutreRouge.Ecrire("Cette fois c'est en rouge");