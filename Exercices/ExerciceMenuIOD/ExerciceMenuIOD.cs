﻿using Menu;

Console.WriteLine("***************************************");
Console.WriteLine("* Menu avec injection de dépendances. *");
Console.WriteLine("***************************************");

//  Création des plats
Entree entree = new("Salade verte");
PlatPrincipal platPrincipal = new("Dos de cabillaud");
Dessert dessert = new("Tiramizu");

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(dessert);
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { new Entree("Nems"),
    new Entree("Giozza"),
    new PlatPrincipal("Bobun"),
    new Dessert("Moshi"),
    new Dessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());

