﻿using Action;
using Player;



PlayerAbstract Barthez = new Gardien(1);
PlayerAbstract Deschamps = new Milieu(11);
PlayerAbstract Pelee = new Ailier(10);
PlayerAbstract Boli = new Defenseur(4);

IPlayingAction Deg = new Degagement();
IPlayingAction Dri = new Drible();
IPlayingAction Pas = new Passe();
IPlayingAction Tet = new Tir();

Console.WriteLine("Munich, 26 mai 1993:\n");

Barthez.Action = Deg;
Barthez.DoAction();

Deschamps.Action = Pas;
Deschamps.DoAction();

Pelee.Action = Dri;
Pelee.DoAction();
Pelee.DoAction();
Pelee.Action = Pas;
Pelee.DoAction();

Boli.Action = Tet;
Boli.DoAction();

Console.WriteLine("\n* Et Marseille est toujours le seul club français champion d'Europe de foot! *");
