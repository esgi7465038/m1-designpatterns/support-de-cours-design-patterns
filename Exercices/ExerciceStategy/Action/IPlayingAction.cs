﻿namespace Action;

internal interface IPlayingAction
{
    public string ActionToDo();
}
