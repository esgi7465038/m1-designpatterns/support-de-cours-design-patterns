﻿using Action;

namespace Player;

internal abstract class PlayerAbstract
{
    public int Numero { get; private set; }

    public IPlayingAction? Action { get; set; }

    protected PlayerAbstract(int numero) => Numero = numero;

    public abstract void DoAction();
}
