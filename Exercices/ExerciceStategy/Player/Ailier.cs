﻿namespace Player;

internal class Ailier : PlayerAbstract
{
    public Ailier(int numero) : base(numero)
    {
    }

    public override void DoAction()
    {
        Console.WriteLine("L'avant n° {0}: {1}", Numero, Action?.ActionToDo());
    }
}
