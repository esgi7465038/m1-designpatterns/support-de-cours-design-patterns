﻿namespace Player;

internal class Gardien : PlayerAbstract
{
    public Gardien(int numero) : base(numero)
    {
    }

    public override void DoAction()
    {
        Console.WriteLine("Le gardien n° {0}: {1}", Numero, Action?.ActionToDo());
    }
}
