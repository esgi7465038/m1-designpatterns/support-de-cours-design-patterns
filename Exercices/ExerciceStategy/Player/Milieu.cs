﻿namespace Player;

internal class Milieu : PlayerAbstract
{
    public Milieu(int numero) : base(numero)
    {
    }

    public override void DoAction()
    {
        Console.WriteLine("Le milieu n° {0}: {1}", Numero, Action?.ActionToDo());
    }
}
