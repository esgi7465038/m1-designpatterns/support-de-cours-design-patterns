﻿namespace Player;

internal class Defenseur : PlayerAbstract
{
    public Defenseur(int numero) : base(numero)
    {
    }

    public override void DoAction()
    {
        Console.WriteLine("Le défenseur n° {0}: {1}", Numero, Action?.ActionToDo());
    }
}
