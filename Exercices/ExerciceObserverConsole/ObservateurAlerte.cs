﻿using Observateur;

namespace ExerciceObserverConsole
{
    internal class ObservateurAlerte : IObservateur
    {
        object verrou = new object();

        public void Actualiser(ISujet sujet)
        {
            lock (verrou)   // Pose un verrou pour exécution complète du bloc
            {
                int val = ((SujetConcret)sujet).ValObservee;

                ConsoleColor oldBg = Console.BackgroundColor;
                ConsoleColor oldFg = Console.ForegroundColor;
                if (val > 75 && val <= 90)
                {
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write("Attention!!");
                }
                else if (val > 90)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("ALERTE!!");
                }
                Console.BackgroundColor = oldBg;
                Console.ForegroundColor = oldFg;
                Console.Write("\n");
            }
        }
    }
}
