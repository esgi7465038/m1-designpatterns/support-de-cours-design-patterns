﻿using ExerciceObserverConsole;

Console.WriteLine("Instanciation du sujet concret, valeur de départ 60");
SujetConcret sujet = new(60);

//  Instanciation des observateurs
ObservateurConcret obs1 = new(ConsoleColor.Green);
ObservateurConcret obs2 = new(ConsoleColor.Red);
ObservateurAlerte obsAlerte = new();

Console.WriteLine("Modification consigne du sujet -> 80");
sujet.Consigne = 80;
Thread.Sleep(3000);

Console.WriteLine("Inscription obs1 (vert): ");
sujet.AjouteObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

Console.WriteLine("Inscription obs2 (rouge) et alerte: ");
sujet.AjouteObservateur(obs2);
sujet.AjouteObservateur(obsAlerte);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

Console.WriteLine("Modification consigne du sujet -> 95");
sujet.Consigne = 95;
Thread.Sleep(10000);

Console.WriteLine("Retrait  obs1: ");
sujet.RetireObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(2000);

Console.WriteLine("Modification consigne du sujet -> 25");
sujet.Consigne = 25;
Thread.Sleep(15000);

sujet.Stop = true;
sujet.Join();

Console.WriteLine("Sujet arrété, retrait des observateurs restants");
sujet.RetireObservateur(obs2);
sujet.RetireObservateur(obsAlerte);
